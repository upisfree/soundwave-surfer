# core/render.coffee
# Делаем стабильный ФПС, независящий (относительно) от устройства (для того, чтобы результаты на разных устройствах не отличались)

Render =
  start: ->
    setTimeout ->
      Render.animate()
    , 1000 / Render.fps
  animate: ->
    requestAnimationFrame Render.animate

    Loop()

    renderer.render stage

  fps: 60