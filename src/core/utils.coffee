# core/utils.coffee
# Всякие утилиты и хелперы

# Math
Math.randomInt = (min, max) ->
  Math.floor Math.random() * (max - min + 1) + min

Math.radiansToDegrees = (r) ->
  r * (180 / Math.PI)

Math.degreesToRadians = (d) ->
  d * (Math.PI / 180)

# Array
Array::first = ->
  this[0]

Array::last = ->
  this[this.length - 1]

Array::min = ->
  Math.min.apply null, this

Array::max = ->
  Math.max.apply null, this

Array::remove = (obj) ->
  for key, value of @
    if value.i == obj.i
      @.splice key, 1

isCollision = (x1, y1, w1, h1, x2, y2, w2, h2) ->
  if ((x1 <= x2 && x1 + w1 >= x2) || (x2 <= x1 && x2 + w2 >= x1)) &&
     ((y1 <= y2 && y1 + h1 >= y2) || (y2 <= y1 && y2 + h2 >= y1))
    true
  else
    false

isSpriteCollision = (s1, s2) ->
  isCollision s1.position.x, s1.position.y, s1.width, s1.height
            , s2.position.x, s2.position.y, s2.width, s2.height
  