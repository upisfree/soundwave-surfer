# core/loop.coffee
# Тик

Loop = ->
  #console.log Voice.update() / 2.5

  # Время
  time += 1
  
  # Обновляем фон
  background.update()

  if Voice.update() / 2 > VOICE_START and IS_GAME_PAUSED
    Start()

  if IS_GAME_PAUSED is false
    # Игрок
    # не доходим до нижней границы?
    #if player.position.y < window.h - player.height - 32
    #  gravity += 20 # продолжаем опускать игрока (ублюдок, мать твою...)

    # не доходим до верхней границы?
    #if player.position.y > -32
    #  gravity -= Voice.update() / 2 + Math.sin(time / 4) # продолжаем поднимать игрока (голос + шероховатости)

    gravity = 20 - Voice.update() / 2 + Math.sin(time / 4)

    player.position.y += gravity
    
    # дошли до нижней границы?
    if player.position.y > window.h + 100
      Fail()

    # дошли до верхней границы?
    if player.position.y < -50 # падать легче
      Fail()

    # # # # # # # # # # # # # # # # #
    # TODO: Абстрактный супер класс #
    # # # # # # # # # # # # # # # # #

    # Еда
    if Math.random() < FOOD_CHANCE
      new Food window.h / 2 + Math.sin(time / SIN_WIDTH) * SIN_HEIGHT
    
    for food in foods.children
      if isSpriteCollision player, food
        foods.removeChild food
        Score.add 1

      if food.position.x <= -food.width
        foods.removeChild food
      else
        food.position.x -= 4 * SPEED
        food.position.y += Math.sin(time / 5)

    # Птицы
    if Math.random() < BIRD_CHANCE
      new Bird
    
    for bird in birds.children
      if isSpriteCollision player, bird
        birds.removeChild bird
        
        Fail()

        console.log 'Bird kills you!'

      if bird.position.x <= -bird.width
        birds.removeChild bird
      else
        bird.position.x -= 4 * SPEED
        bird.position.y += Math.sin(time / 10)

    # Столбы
    if Math.random() < COLUMN_CHANCE
      new Column
    
    for column in columns.children
      for sprite in column.children # пробегаемся по всем частям столба
        if isSpriteCollision(player, sprite) and sprite isnt column.children[column.children.length - 1] # игнорируем столкновения с пеной
          columns.removeChild column
          
          Fail()

          console.log 'Column kills you!'

        if sprite.position.x <= -sprite.width
          columns.removeChild column
        else
          sprite.position.x -= 3 * SPEED