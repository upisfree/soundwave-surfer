# core/voice.coffee
# Работа с голосом

Voice =
  start: ->
    Voice._setPrefixes()
    
    if Voice._supportGetUserMedia()
      navigator.getUserMedia
        'audio':
          'mandatory':
            'googEchoCancellation': true
            'googNoiseSuppression': true
            'googHighpassFilter': true
            'googAutoGainControl': true
      , Voice.analyze
      , Voice.error
    else
      alert 'Can\'t find navigator.getUserMedia()!'

  analyze: (stream) ->
    window.AudioContext = window.AudioContext || window.webkitAudioContext
    audioContext = new AudioContext()

    mediaStreamSource = audioContext.createMediaStreamSource stream

    Voice.analyser = audioContext.createAnalyser()
    Voice.analyser.fftSize = 2048
    mediaStreamSource.connect Voice.analyser

  update: ->
    array = new Uint8Array Voice.analyser.frequencyBinCount
    Voice.analyser.getByteFrequencyData array

    average = 0
    average += parseFloat i for i in array
    average = average / array.length

    # Debug
    return average

  error: (e) ->
    alert e

  _setPrefixes: ->
    navigator.getUserMedia = navigator.getUserMedia ||
                             navigator.webkitGetUserMedia ||
                             navigator.mozGetUserMedia ||
                             navigator.msGetUserMedia
  _supportGetUserMedia: ->
    return !!navigator.getUserMedia

  analyser: null