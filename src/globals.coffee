# globals.coffee
# Здесь описаны всякие глобальные переменные

# пути
PATH = {}
PATH.ASSETS = './assets/'
PATH.TEXTURES = PATH.ASSETS + 'textures/'

# алиасы для ширины, высоты окна
window.w = window.innerWidth
window.h = window.innerHeight

# регулирует ширину синусоиды, по которой появляется еда
# просто число, не в пикселях, это делитель
SIN_WIDTH = 20

# регулирует высоту синусоиды
# просто число, не в пикселях, это множитель
SIN_HEIGHT = 50

# множитель скорости. работает для всего: волн, облаков, еды, какашек...
SPEED = 1

FOOD_CHANCE = 0.005 # шанс появления еды
BIRD_CHANCE = 0.005 # ...птицы
COLUMN_CHANCE = 0.005 # ...колонны

# флаг паузы / старта
IS_GAME_PAUSED = true

# как громко нужно закричать для начала?
VOICE_START = 30