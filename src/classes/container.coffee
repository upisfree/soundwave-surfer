# classes/container.coffee
# Алиас для new PIXI.Container (поддерживается любой другой контейнер, например, PIXI.ParticleContainer)

class Container
  constructor: (type = PIXI.Container, container = stage) ->
    c = new type
    
    container.addChild c

    return c