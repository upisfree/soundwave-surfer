# classes/sprite.coffee
# Алиас для new PIXI.Sprite (поддерживается не только PIXI.Sprite)

class Sprite
  constructor: (type, container, texture, options) ->
    s = new type PIXI.Texture.fromImage texture

    for k, v of options
      s[k] = v

    container.addChild s

    return s