# постоянно растущее значение, которое помогает в Math.sin и Math.cos
time = 0

# инициализурем pixi.js
renderer = new PIXI.autoDetectRenderer window.w, window.h,
  backgroundColor: 0x7adbfe

document.body.appendChild renderer.view

stage = new PIXI.Container

# инициализурем всё что есть
background = new Background

player = null

startMenu = new StartMenu
failMenu = null

birds = new Container
columns = new Container
foods = new Container

Render.start()
Voice.start()

Score.hide()

Start = ->
  # убираем меню фейла, если есть
  if failMenu?
    failMenu.hide()

  # создаём игрока...
  stage.removeChild player
  player = new Player

  # ...а уже потом флаг паузы / старта, чтобы первый цикл в core/loop.coffee не сломался без игрока
  IS_GAME_PAUSED = false

  Score.show()
  Score.zeroing()

  startMenu.hide()

  # обнуляем константы, которые, возможно, изменились
  SIN_WIDTH = 20
  SIN_HEIGHT = 50

  SPEED = 1

  FOOD_CHANCE = 0.005
  BIRD_CHANCE = 0.005
  COLUMN_CHANCE = 0.005

  setTimeout ->
    SPEED = 1.25

    SIN_WIDTH = 10
    SIN_HEIGHT = 200

    BIRD_CHANCE = 0.0075

    setTimeout ->
      SPEED = 1.5

      COLUMN_CHANCE = 0.0075

      setTimeout ->
        SPEED = 1.75
      , 10000
    , 10000
  , 10000

Fail = ->
  # останавливаем мир
  IS_GAME_PAUSED = true

  # убираем игрока
  stage.removeChild player
  player = null

  # убираем «врагов», еду  
  for bird in birds.children
    birds.removeChild bird

  for column in columns.children
    columns.removeChild column

  for food in foods.children
    foods.removeChild food

  # скрываем счёт
  Score.hide()

  # менюшку показываем
  failMenu = new FailMenu