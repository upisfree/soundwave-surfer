# game/obstacles/bird.coffee
# Класс птицы

class Bird
  constructor: (y = Math.randomInt(50, 150)) ->
    return new Sprite PIXI.Sprite, birds, TEXTURE.FLYING_BIRD,
      width: 50
      height: 40
      position:
        x: window.w + 100
        y: y