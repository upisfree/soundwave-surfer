# game/obstacles/column.coffee
# Класс колонны

class Column
  constructor: ->
    c = new Container PIXI.Container, columns
    x = window.w + 100
    y = window.h - 50

    # Спауним не сверху вниз, а снизу вверх (за исключением пены), дабы позиция была правильной
    for i in [1..if Math.random() < 0.25 then 2 else 1]
      new Sprite PIXI.Sprite, c, TEXTURE.COLUMN_PART, # части
        width: 23 # текстры верхушки и части разные, блин
        height: 100
        position:
          x: x # что это вообще, как оно работает?
          y: y - 100 * i

    new Sprite PIXI.Sprite, c, TEXTURE.COLUMN_TOP, # верхуяшка
      width: 28
      height: 20
      position:
        x: x - 2.5
        y: c.children.last().position.y - 20

    if Math.random() < 0.25
      new Sprite PIXI.Sprite, c, TEXTURE.SITTING_BIRD, # сидящая птичка
        width: 35
        height: 35
        position:
          x: x - 12.5
          y: c.children.last().position.y - 35

    new Sprite PIXI.Sprite, c, TEXTURE.FOAM, # пена
      width: 75
      height: 60
      position:
        x: x - 30
        y: y - 40

    c.position.y = 0

    return c