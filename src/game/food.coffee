# game/food.coffee
# Класс еды

class Food
  constructor: (y = Math.randomInt(100, window.h - 60)) ->
    return new Sprite PIXI.Sprite, foods, TEXTURE['FOOD_' + Math.randomInt(1, 3)],
      width: 40
      height: 60
      position:
        x: window.h + Math.randomInt(40, 100)
        y: y