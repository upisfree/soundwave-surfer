# game/player.coffee
# Класс игрока

class Player
  constructor: ->
    c = new Container

    new Sprite PIXI.Sprite, c, TEXTURE.BOARD, # доска
      width: 125
      height: 15
      position:
        x: 8
        y: 75

    new Sprite PIXI.Sprite, c, TEXTURE.SURFER, # сёрфер
      width: 50
      height: 80
      position:
        x: 40
        y: 0

    return c

    #return new Sprite PIXI.Sprite, stage, TEXTURE.SURFER,
    #  width: 175
    #  height: 175
    #  position:
    #    x: 16
    #    y: window.h / 2