# game/background.coffee
# Создание и обновления фона: облаков, волн, дальних объектов (корабль, маяк...)

class Background
  constructor: ->
    @container = new Container

    # Дальние штуки
    @farObjects = new Container PIXI.Container, @container
    @farObjects.textures = # обозначим, какие текстуры должны появляться
    [
      TEXTURE.ISLAND_BIG
      TEXTURE.ISLAND_SMALL
      TEXTURE.LIGHTHOUSE
      TEXTURE.SHIP
      TEXTURE.TANKER
    ]

    # Небо
    @sky = new Sprite PIXI.Sprite, @container, TEXTURE.SKY_2,
      width: window.w
      height: 100
      position:
        x: 0
        y: 285

    # Облака
    @clouds = new Container PIXI.Container, @container

    for i in [0..2]
      c = new Sprite PIXI.Sprite, @clouds, TEXTURE.CLOUD,
        width: 100
        height: 35
        position:
          x: Math.randomInt 0, window.w
          y: Math.randomInt 0, 200

    # Далняя волна
    @waveFarthest = new Sprite PIXI.extras.TilingSprite, @container, TEXTURE.WAVE_FARTHEST,
      width: window.w
      height: 112 # размер текстуры
      position:
        x: 0
        y: 325

    # Средняя волна
    @waveMedium = new Sprite PIXI.extras.TilingSprite, @container, TEXTURE.WAVE_MEDIUM,
      width: window.w
      height: 321 # размер текстуры
      position:
        x: 0
        y: 400

    # Ближайшая волна
    @waveNearest = new Sprite PIXI.extras.TilingSprite, @container, TEXTURE.WAVE_NEAREST,
      width: window.w
      height: 179 # размер текстуры
      position:
        x: 0
        y: window.h - 150

    # Волна под игроком
    @wavePlayer = new Sprite PIXI.Sprite, @container, TEXTURE.WAVE_PLAYER,
      width: 150
      height: 200 # размер текстуры
      position:
        x: -25
        y: window.h

    # Пузырьки
    @bubbles = new Container PIXI.ParticleContainer, @container

    #for i in [0..25]
    #  new Sprite PIXI.Sprite, @bubbles, TEXTURE.BUBBLE,
    #    alpha: 0.5
    #    width: 5
    #    height: 5
    #    position:
    #      x: Math.randomInt 0, window.w
    #      y: Math.randomInt 0, 80

    # Солнышко
    @sun = new Sprite PIXI.Sprite, @container, TEXTURE.SUN,
      width: 100
      height: 100
      position:
        x: window.w
        y: 25
      anchor:
        x: 0.5
        y: 0.5

  update: ->
    # Облака
    for cloud in @clouds.children
      if cloud.position.x <= -cloud.width # если дошли до края окна правой границей (полностью скрылись), перемещаемся обратно в начало
        cloud.position.x = window.w
        cloud.position.y = Math.randomInt 0, 200

      cloud.position.x -= 0.5 * SPEED

    # Дальняя волна
    @waveFarthest.tilePosition.x -= 0.5 * SPEED
    @waveFarthest.position.y += Math.sin(time / 4) / 6

    # Средняя волна
    @waveMedium.tilePosition.x -= 2 * SPEED
    @waveMedium.position.y += Math.sin(time / 6) / 2

    # Ближайшая волна
    @waveNearest.tilePosition.x -= 5 * SPEED
    @waveNearest.position.y += Math.sin(time / 6) / 2

    # Волна под игроком
    if player? # смотрим, чтобы игрок был уже создан или прячем
      @wavePlayer.position.y = player.position.y + player.height - 25
    else
      @wavePlayer.position.y = window.h

    # Всякие объекты сзади
    if Math.random() < 0.001
      s = new Sprite PIXI.Sprite, @farObjects, @farObjects.textures[Math.randomInt(0, @farObjects.textures.length - 1)],
        position:
          x: window.w + 10

      s.width /= 2
      s.height /= 2
      s.position.y = 300 - s.height - 10

    for obj in @farObjects.children
      if obj.position.x <= -obj.width # если скрылись за границей — убираем
        @farObjects.removeChild obj
      else
        obj.position.x -= 0.5 * SPEED