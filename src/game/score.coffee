# game/score.coffee
# Все операции с очками

Score =
  add: (n) ->
    Score.count += n
    Score.update()
  sub: (n) ->
    Score.count -= n
    Score.update()
  zeroing: ->
    Score.count = 0
    Score.update()
  update: ->
    Score.element.textContent = Score.count
  show: ->
    Score.element.style.display = 'block'
  hide: ->
    Score.element.style.display = 'none'
  count: 0
  element: document.getElementById 'score'