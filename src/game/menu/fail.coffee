class FailMenu
  constructor: ->
    @container = new Container

    # а фильтры-то мы уже в начале (стартовое меню) создали
    #background.container.filters[0].blurX =
    #background.container.filters[0].blurY = 4

    blur = new PIXI.filters.BlurFilter
    blur.blurX = blur.blurY = 4

    background.container.filters = [blur]

    # добавляем элементы
    new Sprite PIXI.Sprite, @container, TEXTURE.MENU_SCORE, # логотипчик
      width: 150
      height: 22.5 # посчитано с пропорций текстуры
      position:
        x: window.w / 2
        y: 125
      anchor:
        x: 0.5
        y: 0.5

    # счёт
    text = new PIXI.Text Score.count,
      font: 'bold 140px Arial'
      fill: 0x000000

    text.position.x = window.w / 2 - text.width / 2
    text.position.y = 140

    @container.addChild text

    new Sprite PIXI.Sprite, @container, TEXTURE.MENU_RESTART, # рот и «заори, чтобы начать заново»
      width: 225
      height: 175
      position:
        x: window.w / 2
        y: window.h - 200
      anchor:
        x: 0.5
        y: 0.5
  hide: ->
    # убираем текстуры
    stage.removeChild @container

    # убираем размытие
    background.container.filters[0].blurX =
    background.container.filters[0].blurY = 0