class StartMenu
  constructor: ->
    @container = new Container

    # делаем фон размытым
    blur = new PIXI.filters.BlurFilter
    blur.blurX = blur.blurY = 4

    background.container.filters = [blur]

    # добавляем элементы
    new Sprite PIXI.Sprite, @container, TEXTURE.LOGO, # логотипчик
      width: 275
      height: 85 # посчитано с пропорций текстуры
      position:
        x: window.w / 2
        y: 175
      anchor:
        x: 0.5
        y: 0.5

    new Sprite PIXI.Sprite, @container, TEXTURE.MENU_START, # рот и «заори, чтобы начать»
      width: 225
      height: 175
      position:
        x: window.w / 2
        y: window.h - 200
      anchor:
        x: 0.5
        y: 0.5

  hide: ->
    # убираем текстуры
    stage.removeChild @container

    # убираем размытие
    background.container.filters[0].blurX =
    background.container.filters[0].blurY = 0

  animation: -> # TODO: нормальный класс для анимаций
    logo = @container.getChildAt 0
    start = @container.getChildAt 1

    # TODO: в общий loop переместить
    interval = setInterval ->
      # поднимаем логотип вверх до упора
      if logo.position.y + logo.height > 0
        logo.position.y -= 16

      # опускаем рот с надписью вниз до упора
      if start.position.y < window.h + start.height
        start.position.y += 16

      # убираем размытие
      if background.container.filters[0].blurX > 0 and # хотя, можно сравнивать только по одной оси
         background.container.filters[0].blurY > 0
        background.container.filters[0].blurX -= 0.1
        background.container.filters[0].blurY -= 0.1

      # убираем интервал
      clearInterval interval
    , 16