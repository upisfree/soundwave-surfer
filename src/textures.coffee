# textures.coffee
# Названия и пути всех текстур

TEXTURE =
  LOGO: 'menu/logo'
  MENU_START: 'menu/start'
  MENU_RESTART: 'menu/restart'
  MENU_SCORE: 'menu/score'
  BUBBLE: 'bubble'
  CLOUD: 'cloud'
  PAUSE: 'pause'
  SKY_1: 'sky/1'
  SKY_2: 'sky/2'
  STRIPS: 'strips'
  SUN: 'sun'
  SURFER: 'surfer'
  BOARD: 'board'
  WAVE_FARTHEST: 'waves/farthest'
  WAVE_MEDIUM: 'waves/medium'
  WAVE_NEAREST: 'waves/nearest'
  WAVE_PLAYER: 'waves/player'
  FOOD_1: 'food/ice-cream-1'
  FOOD_2: 'food/ice-cream-2'
  FOOD_3: 'food/ice-cream-3'
  FLYING_BIRD: 'obstacles/flying-bird'
  SITTING_BIRD: 'obstacles/sitting-bird'
  COLUMN_TOP: 'obstacles/column-top'
  COLUMN_PART: 'obstacles/column-part'
  FOAM: 'obstacles/foam'
  ISLAND_BIG: 'background/island-big'
  ISLAND_SMALL: 'background/island-small'
  LIGHTHOUSE: 'background/lighthouse'
  SHIP: 'background/ship'
  TANKER: 'background/tanker'

FORMAT = '.png'

for k, v of TEXTURE
  TEXTURE[k] = PATH.TEXTURES + v + FORMAT

# сразу написать?